Vue.component('cbutton1',{
    template:'<div class="footer">\
    <button class="btn2" v-on:click="add":disabled="this.picked===0">下一步</button>\
    <button class="btn" v-on:click="reset">重置</button></div>',
    data:function(){
        return{
            number:1,
            picked:0
        }
    },
    methods:{
        add:function(){
            this.number++;
            this.$emit('getnumber',this.number)
        },
        reset:function(){
            this.number=1;
            this.$emit('getnumber',this.number)
        }
    },
    mounted:function(){
        var _this=this;
        bus.$on('on-message',function(msg){
            _this.picked=msg;
        })
    }
})

Vue.component('cbutton2',{
    template:'<div class="footer">\
    <button class="btn2" v-on:click="add" :disabled="this.leng===0||this.leng<2||this.leng>3">下一步</button>\
    <button class="btn" v-on:click="sub">上一步</button>\
    <button class="btn" v-on:click="reset">重置</button></div>',
 data:function(){
     return{
         number:2,
         leng:0
     }
 },
 mounted:function(){
    var _this=this;
    bus.$on('len-message',function(msg){
        _this.leng=msg;
    })
 },
 methods:{
     add:function(){
         this.number++;
         this.$emit('getnumber',this.number);
     },
     sub:function(){
         this.number--;
         this.$emit('getnumber',this.number);
     },
     reset:function(){
        this.number=1;
        this.$emit('getnumber',this.number)
    }
 }
})
Vue.component('cbutton3',{
    template:'<div class="footer">\
    <button class="btn" v-on:click="sub">上一步</button>\
    <button class="btn2" :disabled="this.len===0||this.len>20">提交</button>\
    <button class="btn" v-on:click="reset">重置</button>\
    </div>',
    data:function(){
        return{
            number:3,
            len:0
        }
    },
    mounted:function(){
        var _this=this;
        bus.$on('mes-len',function(msg){
            _this.len=msg;
        })
    },
    methods:{
        sub:function(){
            this.number--;
            this.$emit('getnumber',this.number);
        },
        reset:function(){
            this.number=1;
            this.$emit('getnumber',this.number)
        }
    }
    
})