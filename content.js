var bus=new Vue();
Vue.component('ccontent1',{
    template:'<div class="con">\
    <slot>\
    <p>1.请问您的性别是?</p>\
    <p><input type="radio" v-model="picked" value=1 v-on:click="changemale">\
    <label>男</label>\
       <input type="radio" v-model="picked" value=2 v-on:click="changefemale">\
    <label>女</label>\
       <input type="radio" v-model="picked" value=3 v-on:click="changeseret">\
    <label>保密</label>\
    </p>\
    </slot>\
    </div>',
    data:function(){
       return{
           picked:0
       } 
    },
    methods:{
        changemale:function(){
            this.picked=1;
            bus.$emit('on-message',this.picked);
        },
        changefemale:function(){
            this.picked=2;
            bus.$emit('on-message',this.picked);
        },
        changeseret:function(){
            this.picked=3;
            bus.$emit('on-message',this.picked);
        }
    }
})

Vue.component('ccontent2',{
    template:'<div class="con">\
    <slot>\
    <p>2.请选择您的兴趣爱好：\
    </p>\
    <p>\
    <input type="checkbox" v-model="checked" value="reading">\
    <label>看书</label>\
    <input type="checkbox" v-model="checked" value="swimming">\
    <label>游泳</label>\
    <input type="checkbox" v-model="checked" value="running">\
    <label>跑步</label>\
    <input type="checkbox" v-model="checked" value="watchingmovie">\
    <label>看电影</label>\
    <input type="checkbox" v-model="checked" value="listenningmusic">\
    <label>听音乐</label>\
    </p>\
    </slot>\
    </div>',
    data:function(){
        return{
          checked:[],
          len:0
        }
    },
    watch:{
        checked:function(){
            this.len=this.checked.length;
            bus.$emit('len-message',this.len);
        }
    }
})

Vue.component('ccontent3',{
    template:'<div class="con">\
    <p>3.请介绍一下自己：\
    </p>\
    <textarea type="text" v-model="message" placeholder="请输入..."></textarea>\
    </div>',
    data:function(){
        return{
            message:'',
            len:0
        }
    },
    watch:{
       message:function(){
         this.len=this.message.length;
         bus.$emit('mes-len',this.len);
       }
    }
})